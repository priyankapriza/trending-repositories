package com.trending.repositories.utils

import android.graphics.Color
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.repositories.ui.model.RepositoriesResponseItem
import com.trending.repositories.ui.view.adapter.RepositoriesAdapter
import java.util.*


@BindingAdapter(value = ["android:recycler_binding"])
fun bindAdapter(
    recyclerView: RecyclerView,
    characters: ArrayList<RepositoriesResponseItem>?
) {
    val adapter = recyclerView.adapter
    characters?.let {
        if (adapter is RepositoriesAdapter) adapter.setRepositoriesList(it)

    }
}


@BindingAdapter(value = ["text_color"]) //customise your name here
fun setTextColor(view: TextView, color: String) {

    view.setTextColor(Color.parseColor(color))
}









