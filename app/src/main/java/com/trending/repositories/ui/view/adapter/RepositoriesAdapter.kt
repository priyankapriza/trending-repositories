package com.trending.repositories.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.repositories.ui.model.RepositoriesResponse
import com.repositories.ui.model.RepositoriesResponseItem
import com.trending.repositories.R


class RepositoriesAdapter(val listener: OnRepositoriesItemClickListener) :
    RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder>(), Filterable {
    private var mData: ArrayList<RepositoriesResponseItem>? = null
    private var mFilteredData: ArrayList<RepositoriesResponseItem>? = null

    init {
        mData = RepositoriesResponse()
        mFilteredData = RepositoriesResponse()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RepositoriesViewHolder {
        return RepositoriesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(
                    parent.context
                ), R.layout.single_item_repositories_adapter, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return mFilteredData?.size!!
    }

    override fun onBindViewHolder(holder: RepositoriesViewHolder, position: Int) {
        holder.Bind(mFilteredData?.get(position)!!)
        holder.onItemClick(mFilteredData?.get(position)!!, listener)
    }

    fun setRepositoriesList(dataList: ArrayList<RepositoriesResponseItem>) {
        println(dataList.size)
        mData = dataList /*as ArrayList<RepositoriesResponseItem>*/
        mFilteredData = dataList /*as RepositoriesResponse*/
        notifyDataSetChanged()
    }

    inner class RepositoriesViewHolder(private val viewDataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root) {

        fun Bind(similarProductsDataModel: RepositoriesResponseItem) {
            viewDataBinding.run {
                setVariable(BR.repositoriesModel, similarProductsDataModel)
                executePendingBindings()
            }
        }

        fun onItemClick(
            repositoriesModel: RepositoriesResponseItem,
            listener: OnRepositoriesItemClickListener
        ) {
            itemView.setOnClickListener {
                listener.onItemClicked(repositoriesModel)
            }
        }
    }

    interface OnRepositoriesItemClickListener {
        fun onItemClicked(repositoriesResponseItem: RepositoriesResponseItem)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    mFilteredData = mData
                } else {
                    val resultList = RepositoriesResponse()
                    for (row in mData!!) {
                        if (row.name.toLowerCase().contains(charSearch.toLowerCase()) ||
                            row.description.toLowerCase().contains(charSearch.toLowerCase()) ||
                            row.author.toLowerCase().contains(charSearch.toLowerCase()) ||
                            row.language.toLowerCase().contains(charSearch.toLowerCase()) ||
                            row.stars.toString().toLowerCase().contains(charSearch.toLowerCase())
                        ) {
                            resultList.add(row)
                        }
                    }
                    mFilteredData = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = mFilteredData
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                mFilteredData = results?.values as ArrayList<RepositoriesResponseItem>?
                notifyDataSetChanged()
            }

        }
    }

}