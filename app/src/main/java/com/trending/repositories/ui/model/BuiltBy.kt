package com.repositories.ui.model

data class BuiltBy(
    val avatar: String,
    val href: String,
    val username: String
)