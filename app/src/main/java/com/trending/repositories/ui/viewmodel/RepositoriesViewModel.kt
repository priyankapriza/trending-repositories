package com.trending.repositories.ui.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.repositories.network.AuthListener
import com.repositories.network.errorhandling.NetworkResponse
import com.repositories.network.networkutils.Resource
import com.repositories.ui.model.RepositoriesResponseItem
import com.trending.repositories.ui.repositories.Repositories
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import java.util.*

class RepositoriesViewModel(application: Application) : AndroidViewModel(application) {
    var job: CompletableJob? = null
    var authListener: AuthListener? = null
    var repositoriesMutableLiveData = MutableLiveData<ArrayList<RepositoriesResponseItem>>()
    val isApiHit: MutableLiveData<Boolean> = MutableLiveData()
    var language: String? = null
    var since: String? = null
    var spokenLanguageCode: String? = null


    val repositoriesResult = isApiHit.switchMap { id ->
        job = Job()
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO + job!!) {
            emit(Resource.loading(data = null))
            try {
                var data = Repositories.invokeRepositoriesApi(language, since, spokenLanguageCode)
                when (data) {
                    is NetworkResponse.Success -> {
                        Repositories.insertDataToDb(application, data.body)
                        emit(
                            Resource.success(data.body)
                        )
                    }
                    is NetworkResponse.ApiError -> emit(
                        Resource.error(
                            data = null,
                            message = "Internal Error has Occured"
                        )
                    )
                    is NetworkResponse.NetworkError -> emit(
                        Resource.error(
                            data = null,
                            message = "Network Error Occurred!"
                        )
                    )
                    is NetworkResponse.UnknownError -> emit(
                        Resource.error(
                            data = null,
                            message = "Unknown Error Occurred!"
                        )
                    )
                }
                job?.complete()

            } catch (exception: Exception) {
                emit(
                    Resource.error(
                        data = null,
                        message = exception.message ?: "Error Occurred!"
                    )
                )
                job?.complete()
            }
        }
    }

    fun handleRepositoriesResponse(repositoriesResponse: ArrayList<RepositoriesResponseItem>) {
        if (repositoriesResponse != null && repositoriesResponse.size > 0) {
            repositoriesMutableLiveData.value = repositoriesResponse
            authListener?.onSucess("Data has been fetched successfully")
        } else {
            authListener?.onFailure("No Data Found")
        }
    }

    fun invokeRepositoriesApi(mlanguage: String?, mSince: String?, mSpokenLanguageCode: String?) {
        language = mlanguage
        since = mSince
        spokenLanguageCode = mSpokenLanguageCode
        isApiHit.value = true
    }

    fun deleteAll(application: Application) {
        Repositories.deleteAll(application)
    }

    fun checkIfDataIsAvailableInDb(application: Application): ArrayList<RepositoriesResponseItem>? {
        var mRepositoriesResponseItem: ArrayList<RepositoriesResponseItem>? = null
        runBlocking {
            val isDataAvailable = Repositories.checkDataInDbExist(application)
            if (isDataAvailable)
                mRepositoriesResponseItem = Repositories.retrieveDataFromDb(application)
        }
        println()
        return mRepositoriesResponseItem
    }

}