package com.trending.repositories.ui.repositories

import android.app.Application
import android.content.Context
import com.repositories.database.DatabaseHelperImpl
import com.repositories.database.RepositoriesDatabase
import com.repositories.database.RepositoriesEntity
import com.repositories.network.RetrofitApiClient
import com.repositories.ui.model.RepositoriesResponseItem
import kotlinx.coroutines.runBlocking

object Repositories {

    suspend fun invokeRepositoriesApi(
        language: String?,
        since: String?,
        spokenLanguageCode: String?
    ) = RetrofitApiClient.getClient().invokeRepositoriesApi(language, since, spokenLanguageCode)

    suspend fun checkDataInDbExist(application: Application): Boolean {
        var isAvailable = false
        val dbHelper = DatabaseHelperImpl(initializeDB(application))
        if (dbHelper.getRepositories() != null && dbHelper.getRepositories().size > 0)
            isAvailable = true
        return isAvailable
    }

    suspend fun insertDataToDb(
        application: Application,
        response: ArrayList<RepositoriesResponseItem>
    ) {
        val dbHelper = DatabaseHelperImpl(initializeDB(application))
        if (response.size > 0) {
            for (i in response) {
                dbHelper.insertRepositories(
                    RepositoriesEntity(
                        0,
                        i.author,
                        i.name,
                        i.avatar,
                        i.url,
                        i.description,
                        i.language,
                        i.languageColor,
                        i.stars
                    )
                )
            }
        }
    }

    fun initializeDB(context: Context): RepositoriesDatabase {
        return RepositoriesDatabase.getInstance(context)
    }

    suspend fun retrieveDataFromDb(application: Application): ArrayList<RepositoriesResponseItem> {
        val repositoriesList: ArrayList<RepositoriesResponseItem> = ArrayList()
        // var mRepositoriesResponse :RepositoriesResponse=RepositoriesResponse()
        val dbHelper = DatabaseHelperImpl(initializeDB(application))
        if (dbHelper.getRepositories().size > 0) {
            for (i in dbHelper.getRepositories()) {
                repositoriesList.add(
                    RepositoriesResponseItem(
                        i.author, i.name,
                        i.avatar, i.url, i.description,
                        i.language, i.languageColor, i.stars
                    )
                )
            }
        }
        return repositoriesList
    }

    fun deleteAll(application: Application) {
        val dbHelper = DatabaseHelperImpl(initializeDB(application))
        runBlocking { dbHelper.deleteAll() }
    }

}

