package com.trending.repositories.ui.view.activities

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.repositories.network.AuthListener
import com.repositories.network.networkutils.Status
import com.repositories.ui.model.RepositoriesResponseItem
import com.trending.repositories.R
import com.trending.repositories.databinding.ActivityRepositoriesBinding
import com.trending.repositories.ui.view.adapter.RepositoriesAdapter
import com.trending.repositories.ui.viewmodel.RepositoriesViewModel
import com.trending.repositories.utils.AppConstants
import com.trending.repositories.widget.ConnectionDetector
import com.trending.repositories.widget.CustomProgressDialog
import kotlinx.android.synthetic.main.activity_repositories.*

class RepositoriesActivity : AppCompatActivity(), AuthListener,
    RepositoriesAdapter.OnRepositoriesItemClickListener, View.OnClickListener {
    lateinit var repositoriesBinding: ActivityRepositoriesBinding
    lateinit var repositoriesViewModel: RepositoriesViewModel
    var language: String? = null
    var since: String? = AppConstants.DAILY
    var spokenLanguageCode: String? = null
    var searchView: SearchView? = null

    val repositoriesAdapter: RepositoriesAdapter by lazy {
        RepositoriesAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initialize()
    }

    private fun initialize() {
        initializeView()
        initRecyclerView()
        setRefreshListener()
        invokeRepositoriesApi()
        observeRepositoriesApi()
    }

    private fun setRefreshListener() {
        swipe_refresh_layout.setOnRefreshListener {
            Handler().postDelayed(Runnable {
                repositoriesViewModel.deleteAll(application)
                swipe_refresh_layout.isRefreshing = false
                invokeRepositoriesApi()
            }, 3000)
        }
    }

    private fun initRecyclerView() {
        repositories_recycler_view.run {
            adapter = repositoriesAdapter
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun initializeView() {
        repositoriesBinding = DataBindingUtil.setContentView(this, R.layout.activity_repositories)
        repositoriesBinding.lifecycleOwner = this
        repositoriesViewModel = ViewModelProvider(this).get(RepositoriesViewModel::class.java)
        repositoriesBinding.repositoriesVm = repositoriesViewModel
    }

    private fun observeRepositoriesApi() {
        repositoriesViewModel.repositoriesResult.observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.LOADING -> {
                        CustomProgressDialog.show(this)
                    }
                    Status.SUCCESS -> {
                        CustomProgressDialog.dialog?.dismiss()
                        resource.data?.let { repositoriesResponse ->
                            repositoriesViewModel.handleRepositoriesResponse(repositoriesResponse)
                        }
                    }
                    Status.ERROR -> {
                        CustomProgressDialog.dialog?.dismiss()
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }

                }
            }
        }
        )
    }

    override fun onStarted() {
    }

    override fun onSucess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onFailure(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onItemClicked(similarProductsDataModel: RepositoriesResponseItem) {
    }


    override fun onBackPressed() {
        if (!searchView!!.isIconified) {
            searchView?.onActionViewCollapsed()
        } else {
            super.onBackPressed()
        }
    }


    override fun onClick(view: View?) {
        if (view?.id == R.id.try_again_btn)
            invokeRepositoriesApi()
    }

    private fun invokeRepositoriesApi() {
        val repositoriesList = repositoriesViewModel.checkIfDataIsAvailableInDb(application)
        if (repositoriesList != null && repositoriesList.size > 0) {
            swipe_refresh_layout.visibility = View.VISIBLE
            no_internet_connection_lin_layout.visibility = View.GONE
            repositoriesViewModel.handleRepositoriesResponse(repositoriesList)
        } else if (ConnectionDetector.isConnectingToInternet(this)) {
            swipe_refresh_layout.visibility = View.VISIBLE
            no_internet_connection_lin_layout.visibility = View.GONE
            repositoriesViewModel.invokeRepositoriesApi(language, since, spokenLanguageCode)
        } else {
            no_internet_connection_lin_layout.visibility = View.VISIBLE
            swipe_refresh_layout.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchManager: SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search)?.actionView as SearchView?
        searchView?.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                repositoriesAdapter.filter.filter(newText)
                return false
            }
        })
        return true
    }

}