package com.repositories.network.networkutils

object NetworkConstants {
    const val BASE_URL = "https://private-anon-b7d3c323b1-githubtrendingapi.apiary-mock.com/"
    const val LANGUAGE = "language"
    const val SINCE = "since"
    const val SPOKEN_LANGUAGE_CODE = "spoken_language_code"
    const val REPOSITORIES_API = "repositories"
}

