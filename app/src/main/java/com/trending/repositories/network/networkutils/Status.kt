package com.repositories.network.networkutils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}