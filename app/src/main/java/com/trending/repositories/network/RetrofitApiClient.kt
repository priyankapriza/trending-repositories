package com.repositories.network

import com.repositories.network.errorhandling.NetworkResponseAdapterFactory
import com.repositories.network.networkutils.NetworkConstants
import retrofit2.Retrofit

object RetrofitApiClient {
    const val BASE_URL: String = NetworkConstants.BASE_URL

    fun getClient() = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(NetworkResponseAdapterFactory())
        .addConverterFactory(RetrofitApiWorker.gsonConverter)
        .client(RetrofitApiWorker.client)
        .build()
        .create(RetrofitApiInterface::class.java)
}


