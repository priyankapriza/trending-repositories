package com.repositories.network

import com.repositories.network.errorhandling.NetworkResponse
import com.repositories.network.networkutils.NetworkConstants
import com.repositories.ui.model.RepositoriesResponseItem
import retrofit2.http.GET
import retrofit2.http.Query

typealias GenericResponse<S> = NetworkResponse<S, S>

interface RetrofitApiInterface {
    @GET(NetworkConstants.REPOSITORIES_API)
    suspend fun invokeRepositoriesApi(
        @Query(NetworkConstants.LANGUAGE) language: String?,
        @Query(NetworkConstants.SINCE) since: String?,
        @Query(NetworkConstants.SPOKEN_LANGUAGE_CODE) spokenLanguageCode: String?
    ): GenericResponse<ArrayList<RepositoriesResponseItem>>

}