package com.repositories.network

interface AuthListener {
    fun onStarted()
    fun onSucess(token: String)
    fun onFailure(message: String)

}