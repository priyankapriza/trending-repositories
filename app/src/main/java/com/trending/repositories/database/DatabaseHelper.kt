package com.repositories.database

interface DatabaseHelper {

    suspend fun getRepositories(): List<RepositoriesEntity>

    suspend fun insertRepositories(repositories: RepositoriesEntity)

    suspend fun deleteAll()
}