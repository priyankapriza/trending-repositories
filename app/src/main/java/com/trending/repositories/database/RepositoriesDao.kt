package com.repositories.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RepositoriesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRepositories(mRepositoriesEntity: RepositoriesEntity)

    @Query("select * FROM repositories")
    suspend fun getRepositories(): List<RepositoriesEntity>

    @Query("DELETE FROM repositories")
    suspend fun deleteAll()
}