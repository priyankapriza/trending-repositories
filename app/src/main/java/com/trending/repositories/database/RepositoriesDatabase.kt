package com.repositories.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [RepositoriesEntity::class], version = 1)
abstract class RepositoriesDatabase : RoomDatabase() {

    abstract val mRespositoryDao: RepositoriesDao

    companion object {
        @Volatile
        private var INSTANCE: RepositoriesDatabase? = null

        fun getInstance(context: Context): RepositoriesDatabase {
            var instance: RepositoriesDatabase? = INSTANCE
            synchronized(this) {
                if (instance == null) {
                    instance = buildRoomDB(context)
                }
            }
            return instance!!
        }

        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                RepositoriesDatabase::class.java,
                "repositories"
            ).build()

    }
}