package com.repositories.database


class DatabaseHelperImpl(private val appDatabase: RepositoriesDatabase) : DatabaseHelper {

    override suspend fun getRepositories(): List<RepositoriesEntity> =
        appDatabase.mRespositoryDao.getRepositories()

    override suspend fun insertRepositories(repositoriesEntity: RepositoriesEntity) =
        appDatabase.mRespositoryDao.insertRepositories(
            repositoriesEntity
        )

    override suspend fun deleteAll() = appDatabase.mRespositoryDao.deleteAll()

}