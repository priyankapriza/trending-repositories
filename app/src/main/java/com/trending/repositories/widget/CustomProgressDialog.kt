package com.trending.repositories.widget

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import com.trending.repositories.R
import kotlinx.android.synthetic.main.progress_dialog_view.view.*


object CustomProgressDialog {

    var dialog: CustomDialog? = null

    fun show(context: Context): Dialog {
        val inflater = (context as Activity).layoutInflater
        val view = inflater.inflate(R.layout.progress_dialog_view, null)

        setColorFilter(
            view.cp_pbar.indeterminateDrawable,
            ResourcesCompat.getColor(context.resources, R.color.blue, null)
        )

        dialog = CustomDialog(context)
        dialog?.setContentView(view)
        dialog?.setCancelable(false)
        dialog?.show()
        return dialog!!
    }

    private fun setColorFilter(drawable: Drawable, color: Int) {
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }

    class CustomDialog(context: Context) : Dialog(context, R.style.CustomDialogTheme)
}